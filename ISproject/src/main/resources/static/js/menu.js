
const HttpMethod = {
	Get: 'GET',
	Post: 'POST',
	Delete: 'DELETE'	
};
 
const makeHttpHelper = function(rootUrl) {
	const parse = async function(response) {
		const responseText = await response.text();
		if(responseText.length > 0) {
			return Promise.resolve(JSON.parse(responseText));
		} else {
			throw new Error('Expected result but has nothing in response');
		}
	}
	const prepareError = async function(response) {
		let errMsg = '';
		
		if (response.headers.get('Content-Type')?.startsWith('application/json') == true) {
			let err = await parse(response);
			while(err) {
				errMsg += err.message + '\n';
				err = err.underlying.length == 0 ? undefined : err.underlying[0];
			}
		} else {
			errMsg = response.statusText;
		}
		console.warn(`Response (status $(response.status) error message: ${errMsg})`);
		throw new Error(errMsg);
	}
	const request = async function(method, url, body) {
		let bodyContent = null;
		const headers = new Headers();
		headers.set('Accept', 'application/json');
		
		if(body !== undefined) {
			if(body instanceof FormData) {
				bodyContent = body;
			} else {
				headers.set('Content-Type', 'application/json');
				bodyContent = JSON.stringify(body);
			}
		}
		
		const response = await fetch(rootUrl + url, {
			body: bodyContent,
			method: method,
			meaders: headers
		});
		
		if (response.ok) {
			return Promise.resolve(response);
		} else {
			prepareError(response);
		}
	}
	
	return {
		getText: 		async function(url) { return (awaitrequest(HttpMethod.Get, url)).text();},
		getAndParse:	async function(url) {return await parse(await request(HttpMethod.Get, url));},
		post:			async function(url, arg) {return request(HttpMethod.Post, url, arg);},
		postAndParse:	async function(url, arg) {return await parse(await request(HttpMethod.Post, url, arg));},
		delete:         async function(url) {return request(HttpMethod.Delete, url);}
	};
};
 
const makeFormDataBuilder = function() {
	const data = new FormData();
	
	const obj = {
		data: data,
		append: function(name, value, filename) {
			data.append(name, value, filename);
			return obj;
		}
	}
	return obj;
} 
 
 
const bindMyApi = function(rootUrl) {
	const http = makeHttpHelper(rootUrl);
	return {
		getAllDishes: function() {return http.getAndParse('/dishes');},
		getPagedDishes: function(page, size) {return http.getAndParse(`/dishes?page=${page}&size=${size}`);},
		getDishById: function(id) {return http.getAndParse('/dishes/' + id)},
		createDish: function(name, description, kcal, price, imageFile) {
			const spec = makeFormDataBuilder()
								.append('name', name)
								.append('description', description)
								.append('kcal', kcal)
								.append('price', price)
								.append('imageFile', imageFile);
			return http.postAndParse('/dishes?action=create', spec.data);								
		},
		updateDish: function(id, name, description, kcal, price, imageFile) {
			const spec = makeFormDataBuilder()
								.append('name', name)
								.append('description', description)
								.append('kcal', kcal)
								.append('price', price)
								.append('imageFile', imageFile);
			return http.postAndParse(`/dishes/${id}?action=update`, spec.data);
		},
		deleteDish: function(id) {return http.delete(`/dishes/${id}`);}
		
	};
};

const makePaginator = function(elementID, loadPage, presentPage) {
	var pgdiv= null;
	var pageNumber = 0;
	var pageSize = 3;
	var pagesCount = 0;
	var collection = null;
	
	const updateControls = function() {
		var result = '';
		
		if (pageNumber > 0) {
			result += `<a href="#" onClick="pages.paginate(0);"> 1 </a>`;
		}
		if(pageNumber > 2) {
			result += `<span>...</span>`;
		}
		if (pageNumber > 1) {
			result += `<a href="#" onClick="pages.paginate(${pageNumber - 1});"> ${pageNumber} </a>`;
		}
		
		result += `<a href="#" onClick="pages.paginate(${pageNumber});"><strong>${pageNumber + 1} </strong></a>`;
		
		if (pageNumber < pagesCount - 2) {
			result += `<a href="#" onClick="pages.paginate(${pageNumber + 1});"> ${pageNumber + 2} </a>`;
		}
		
		if(pageNumber > pagesCount - 3) {
			result += `<span>...</span>`;
		}
		
		if (pageNumber < pagesCount - 1) {
			result += `<a href="#" onClick="pages.paginate(${pagesCount - 1});"> ${pagesCount} </a>`;
		}
		
		pgdiv.innerHTML = result;
		
	};
	
	const updateData = async function(page) {
		collection = await loadPage(page, pageSize);
		pagesCount = collection.totalPages;
		pageNumber = collection.page.number;
		updateControls();
		await presentPage(collection.items);
		return false;
	};
	
	return {
		setup: async function() { pgdiv = document.getElementById(elementID); updateData(0); },
		paginate: updateData,
		refresh: async function() {updateData(pageNumber);}
	};
}


var entries = [];//items presented at the moment

function makeEntryView(dish) {
	var result = `
	 	<td>${dish.id}</td>
	 	<td>${dish.name}</td>
	 	<td>${dish.price}</td>
	 	<td>${dish.kcal}</td>
	 	<td>${dish.description}</td>
	 	<td>--</td><!-- TODO -->
	 	<td>`;
	 	
	if(dish.imageId) {
		result += `<img src="/dishes/${dish.id}/image " width="100" height="100"/>`;
	} else {
		result +='No image';
	}
	 		
	   result += `</td>
	 	<td>
	 		<a href="/dishes/${dish.id}?action=edit&mode=htmlForm">Edit</a>
	 		<a href="#" onClick="deleteEntry(${dish.id});">Delete</a>
	 	</td>
	`;
	return result;
}

const api = bindMyApi('');
const pages = makePaginator(
	'paginator',
	async function(n, s) {return api.getPagedDishes(n, s);},
	async function(items) {
		const container = document.getElementById('container');
		entries.forEach(function(e) {container.removeChild(e); });
		entries = [];
		items.forEach(function(dish) {
			const e = container.appendChild(document.createElement('tr'));
			entries.push(e);
			e.innerHTML = makeEntryView(dish);
		});
	}
);

async function deleteEntry(id) {
	await api.deleteDish(id);
	await pages.refresh();
	return false;
} 

 
 
window.onload = function() {
	pages.setup();
	
}
 
 
 
 
 
 
 
 
 