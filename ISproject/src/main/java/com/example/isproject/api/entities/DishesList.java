package com.example.isproject.api.entities;

import java.util.List;

import org.springframework.data.domain.Page;

import com.example.isproject.db.entities.Dish;
import com.example.isproject.internals.PageSpec;

public class DishesList {

    public class PageInfo {
        public final PageSpec pageSpec;
        public final long number, size;

        public PageInfo(PageSpec spec, long number, long size) {
            super();
            pageSpec = spec;
            this.number = number;
            this.size = size;
        }

        public PageSpec getSpec() {
            return pageSpec;
        }

        public long getNumber() {
            return number;
        }

        public long getSize() {
            return size;
        }
    }

    private final PageInfo pageInfo;
    private final long totalItems, totalPages;
    private final List<Dish> items;

    private DishesList(PageSpec spec, Page<Dish> page) {
        pageInfo = spec == null || !spec.applied ? null : new PageInfo(spec, page.getNumber(), page.getSize());
        totalItems = page.getTotalElements();
        totalPages = page.getTotalPages();
        items = page.getContent();
    }


    public static DishesList forResultOf(PageSpec spec, Page<Dish> result) {
        return new DishesList(spec, result);
    }


    public PageInfo getPage() {
        return pageInfo;
    }

    public long getTotalItems() {
        return totalItems;
    }

    public long getTotalPages() {
        return totalPages;
    }

    public List<Dish> getItems() {
        return items;
    }
}
