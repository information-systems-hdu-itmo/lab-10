package com.example.isproject.api.entities;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ErrorInfo {
    public final String exceptionKind;
    public final String message;
    public final ErrorInfo[] underlying;
    public final String stackTraceString;


    public ErrorInfo(Throwable ex) {
        StringWriter writer = new StringWriter();
        ex.printStackTrace(new PrintWriter(writer));


        this.exceptionKind = ex.getClass().getName();
        this.message = ex.getMessage();
        this.stackTraceString = writer.getBuffer().toString();

        if (ex.getCause() == null) {
            this.underlying = new ErrorInfo[0];
        } else {
            this.underlying = new ErrorInfo[]{new ErrorInfo(ex.getCause())};
        }
    }
}
